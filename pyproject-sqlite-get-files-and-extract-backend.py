"""
https://docs.python.org/3.9/library/sqlite3.html
https://www.sqlite.org/lang_datefunc.html

$ sqlite3 extract-pyproject-latest.db

sqlite> .schema
CREATE TABLE pyprojects (
  repository TEXT,
  project_name TEXT,
  project_version TEXT,
  nb_uploads INTEGER,
  uploaded_on TEXT,
  year INTEGER,
  path TEXT
);

sqlite> .mode table
sqlite> select DISTINCT project_name, project_version, nb_uploads, uploaded_on, year from pyprojects order by nb_uploads desc limit 10;
+-------------------------------+-----------------+------------+-------------------------+------+
|         project_name          | project_version | nb_uploads |       uploaded_on       | year |
+-------------------------------+-----------------+------------+-------------------------+------+
| OZI                           | 0.0.219         | 7304       | 2023-11-24 23:59:27.615 | 2023 |
| teamhack-nmap                 | 0.0.4328        | 4140       | 2023-11-21 07:13:54.906 | 2023 |
| ddtrace                       | 2.3.1           | 3804       | 2023-11-22 20:07:23.221 | 2023 |
| pdm                           | 2.10.4          | 1672       | 2023-11-24 01:43:35.052 | 2023 |
| cdktf-cdktf-provider-newrelic | 11.0.4          | 1657       | 2023-11-09 03:18:09.907 | 2023 |
| utilmy                        | 0.1.17009007    | 1500       | 2023-11-25 08:26:46.145 | 2023 |
| poetry-core                   | 1.8.1           | 1440       | 2023-10-31 16:03:40.219 | 2023 |
| poetry                        | 1.7.1           | 1409       | 2023-11-16 19:09:08.238 | 2023 |
| pepperize.cdk-organizations   | 0.7.742         | 1381       | 2023-11-23 00:16:04.116 | 2023 |
| coiled                        | 1.1.16.dev4     | 1374       | 2023-11-23 19:49:42.64  | 2023 |
+-------------------------------+-----------------+------------+-------------------------+------+
"""

import logging
import sqlite3
import re
import time
import tomli
import pycodeorg

LOG = logging.getLogger(__name__)


if __name__ == "__main__":
    start_time = time.time()

    logging.basicConfig(filename="pyproject-backends.log", level=logging.INFO)

    # Create backend table
    # --------------------
    with open("create-table-backend.sql", "r") as f:
        CREATE_BACKEND = f.read()

    cnx_backend = sqlite3.connect("pyproject_backends.db")
    cur_backend = cnx_backend.cursor()

    cur_backend.execute(CREATE_BACKEND)

    # Get project data
    # ----------------
    with open("query-projects.sql", "r") as f:
        QUERY_PROJECTS = f.read()

    cnx_proj = sqlite3.connect("extract-pyproject-latest.db")
    cnx_proj.row_factory = sqlite3.Row
    cur_proj = cnx_proj.cursor()

    cur_proj.execute("SELECT COUNT(*) AS nb FROM pyprojects;")
    r = cur_proj.fetchone()
    total = r["nb"]
    cpt = 0

    for row in cur_proj.execute(QUERY_PROJECTS):
        values = {
            "repository": row["repository"],
            "project_name": row["project_name"],
            "project_version": row["project_version"],
            "nb_uploads": row["nb_uploads"],
            "uploaded_on": row["uploaded_on"],
            "year": row["year"],
            "path": row["path"],
        }

        # Only fetch the pyproject.toml at the root of the project
        # --------------------------------------------------------
        parts = values["path"].split("/")
        if len(parts) == 5 and parts[-1] == "pyproject.toml":
            # Fetch the file data from the dataset
            # ------------------------------------
            try:
                data = pycodeorg.get_data(
                    values["repository"], values["project_name"], values["path"]
                )
            except ValueError as e:
                LOG.error(
                    "pycodeorg.get_data failed to retrieve %s: '%s'"
                    % (values["project_name"], e)
                )
                continue

            # Then get the 'build-backend' value with a toml library
            # ------------------------------------------------------
            try:
                toml_dict = tomli.loads(data.decode())
            except (tomli.TOMLDecodeError, UnicodeDecodeError) as e:
                LOG.error(
                    "Error reading TOML file for %s: '%s'" % (values["project_name"], e)
                )
                continue

            # print(f"{toml_dict=}")

            if toml_dict.get("build-system") and toml_dict["build-system"].get(
                "build-backend"
            ):
                backend = toml_dict["build-system"].get("build-backend")

                values["backend"] = backend
                print(f"{values['project_name']} : {values['backend']}")
            else:
                values["backend"] = None
                print(f"{values['project_name']} : .......... no backend found")

            try:
                cur_backend.execute(
                    """INSERT INTO backends 
                              VALUES (:repository, :project_name, :project_version,
                                      :backend, :nb_uploads, :uploaded_on, :year, :path)
                    """,
                    values,
                )
                cnx_backend.commit()
            except sqlite3.InterfaceError as e:
                LOG.error(
                    "Error writing to sqlite3 for %s: '%s'"
                    % (values["project_name"], e)
                )
                continue

        else:
            LOG.info(
                f"%s is not a root path for %s"
                % (values["path"], values["project_name"])
            )

        cpt = cpt + 1
        if cpt % 2500 == 0:
            LOG.info("PROGRESS: %d / %d, %.2f %%" % (cpt, total, cpt * 100 / total))

    cnx_proj.close()
    cnx_backend.close()

    end_time = time.time()

    duration_msg = f"Getting backends took : {end_time - start_time:0.3} seconds."

    LOG.info(duration_msg)
    print(duration_msg)
