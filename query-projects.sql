-- Define the dialect
-- sqlfluff:dialect:sqlite

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

SELECT
  repository,
  project_name,
  project_version,
  nb_uploads,
  uploaded_on,
  year,
  path
FROM pyprojects;
