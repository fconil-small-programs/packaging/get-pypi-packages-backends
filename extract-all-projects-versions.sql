-- Define the dialect
-- sqlfluff:dialect:duckdb

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

SELECT
  project_name,
  COUNT(project_name) AS nb_uploads,
  MAX(project_version) AS max_version,
  LIST(DISTINCT project_version) AS all_versions,
  MAX(uploaded_on) AS max_uploaded_on,
  LIST(DISTINCT uploaded_on) AS all_uploaded_on,
  LIST(DISTINCT repository) AS all_repository,
  LIST(DISTINCT path) AS all_path
FROM '*.parquet'
WHERE
  (DATE_PART('year', uploaded_on) >= '2018')
  AND REGEXP_MATCHES(path, 'pyproject.toml$')
  AND skip_reason = ''
GROUP BY project_name;
