"""
https://sethmlarson.dev/security-developer-in-residence-weekly-report-18

https://duckdb.org/docs/sql/query_syntax/with
https://duckdb.org/docs/sql/functions/timestamp
https://duckdb.org/docs/sql/aggregates
https://duckdb.org/docs/guides/python/execute_sql
"""

import duckdb

with open('extract-all-projects-versions.sql', 'r') as f:
    ALL_VERSIONS_QUERY = f.read()

res = duckdb.sql(ALL_VERSIONS_QUERY)

res.to_csv("extract-pyproject-all-versions.csv", header=True)

with open('extract-latest-project-version.sql', 'r') as f:
    LATEST_QUERY = f.read()

# res = duckdb.sql(LATEST_QUERY).show()

res = duckdb.sql(LATEST_QUERY)
res.to_csv("extract-pyproject-latest.csv", header=True)
