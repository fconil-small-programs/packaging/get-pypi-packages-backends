-- Define the dialect
-- sqlfluff:dialect:duckdb

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

WITH lpv AS (
  SELECT
    project_name,
    COUNT(project_name) AS nb_uploads,
    MAX(uploaded_on) AS max_uploaded_on,
    LIST(DISTINCT uploaded_on) AS all_uploaded_on
  FROM '*.parquet'
  WHERE
    (DATE_PART('year', uploaded_on) >= '2018')
    AND REGEXP_MATCHES(path, 'pyproject.toml$')
    AND skip_reason = ''
  GROUP BY project_name
)

SELECT
  ip.repository,
  ip.project_name,
  ip.project_version,
  lpv.nb_uploads,
  ip.uploaded_on,
  DATE_PART('year', ip.uploaded_on) AS year,
  ip.path
FROM '*.parquet' AS ip
JOIN
  lpv
  ON ip.project_name = lpv.project_name AND ip.uploaded_on = lpv.max_uploaded_on
WHERE REGEXP_MATCHES(path, 'pyproject.toml$') AND skip_reason = '';
