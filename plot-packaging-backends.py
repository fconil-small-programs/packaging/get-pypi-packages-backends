"""
https://docs.python.org/3.9/library/sqlite3.html
https://www.sqlite.org/uri.html : file path, URI and read-only mode

https://matplotlib.org/2.0.2/users/pyplot_tutorial.html
https://matplotlib.org/2.0.2/examples/api/barchart_demo.html
"""

from pathlib import Path
import sqlite3
import matplotlib.pyplot as plt

BACKEND_PATH = Path("~/Progs/python/duckdb/pyproject_backends.db")

QUERY_COUNT = "SELECT COUNT(*) AS nb FROM backends WHERE backend IS NOT NULL;"

# QUERY = "SELECT backend, COUNT(backend) AS nb FROM backends WHERE backend IS NOT NULL GROUP BY backend;"

QUERY = """SELECT backend, COUNT(backend) AS nb 
FROM backends 
WHERE backend IS NOT NULL 
GROUP BY backend 
HAVING COUNT(backend) > 5;
"""

if __name__ == "__main__":
    cnx = sqlite3.connect(f"file:{str(BACKEND_PATH.expanduser())}?mode=ro")
    cnx.row_factory = sqlite3.Row
    cur = cnx.cursor()

    cur.execute(QUERY_COUNT)
    r = cur.fetchone()

    backends_total = r['nb']

    cur.execute(QUERY)

    r = cur.fetchall()

    backends = [ t[0] for t in r ]
    backend_nb = [ t[1] for t in r ]

    cnx.close()

    fig = plt.figure(num='Backends on PyPI', figsize=(12,8), dpi=600)
    ax = fig.add_subplot(1, 1, 1)

    bars = ax.bar(backends, backend_nb, color=['blue' if n > 500 else 'cyan' for n in backend_nb])

    ax.set_title(f'{backends_total} backends declared in pyproject.toml on PyPI (2018-2023), removing nb < 5, log scale')
    ax.set_xlabel("Backend")
    ax.set_ylabel("Times declared")
    
    """ 
    TODO : .../plot-packaging-backends.py:62:
    UserWarning: set_ticklabels() should only be used with a fixed number of
    ticks, i.e. after set_ticks() or using a FixedLocator.
    """

    # Rotating x-axis labels for better readability
    ax.set_xticklabels(backends, rotation=70, ha='right')

    # Add y value labels above the bars
    for bar, nb in zip(bars, backend_nb):
        ax.text(bar.get_x() + bar.get_width() / 2, bar.get_height() + 0.5, str(nb), ha='center', va='bottom')

    # Set logarithmic scale on y-axis
    # ax.set_yscale('log')

    # Color x-axis labels based on the condition
    for label, n in zip(ax.get_xticklabels(), backend_nb):
        if n > 500:
            label.set_color('blue')
    
    # Adjust layout to prevent clipping of rotated labels
    plt.tight_layout()

    #plt.show()
    plt.savefig("python-backends-2018-2023.png")

