# Get PyPI packages backends

Get Python packages backends found in `pyproject.toml` files with the
`build-backend` key from [PyPI](https://pypi.org/).

This could be done thanks to [Querying every file in every release on the Python Package Index](https://sethmlarson.dev/security-developer-in-residence-weekly-report-18)
from Seth Michael Larson.

The `pyproject-sqlite-get-files-and-extract-backend.py` script makes use of
Seth Michael Larson gist : [pycodeorg.py](https://gist.github.com/sethmlarson/852341a9b7899eda7d22d8c362c0a095)

You can find the data that was extracted to produce the 
[backend charts](https://discuss.python.org/t/backends-declared-in-the-pyproject-toml-files-during-2018-to-2023-on-pypi/40629)
on the dataset [PyPI projects backends](https://datasets.liris.cnrs.fr/pypi-projects-backends-version1)
(CSV and SQLite files).

