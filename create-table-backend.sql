-- Define the dialect
-- sqlfluff:dialect:sqlite

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

CREATE TABLE IF NOT EXISTS backends
(
  repository TEXT,
  project_name TEXT,
  project_version TEXT,
  backend TEXT,
  nb_uploads INTEGER,
  uploaded_on TEXT,
  year INTEGER,
  path TEXT
);
